---
layout: handbook-page-toc
title: "UX Research Coordination"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Research Coordination at GitLab

Research Coordinators at GitLab are responsible for managing all aspects of participant recruitment for GitLab’s user experience research studies including, but not limited to, sourcing, outreach, screening, scheduling, participation agreements, and incentives management. 

To request support from a Research Coordinator, please follow the [process outlined here](/handbook/engineering/ux/ux-research/#how-ux-research-product-and-product-design-work-together-on-research).

### Recruitment methods

1. **GitLab First Look (formerly the UX Research Panel)** is a group of users who have opted in to receive research studies from GitLab. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html). This is a good fit for studies that are aimed at GitLab users who are software developers and related roles. The panel currently does not have many security professionals, nor senior engineering leaders. 

1. **Respondent.io** is a recruitment service that is a good choice for studies aimed at software professionals who are not necessarily GitLab users. This has been a decent source of security professionals and some other harder-to-reach users. 

1. **Social outreach.** Social posts may go out on GitLab's brand channels. The Research Coordinator uses the Social Request template in the corporate marketing project to request this type of post. This is a good choice for studies primarily aimed at GitLab users. Product Managers, Product Designers, and other teammates are highly encouraged to help promote their studies to their networks.

### Incentives

* User interviews, usability testing, or 'buy a feature' research: $60 (or equivalent currency) Amazon gift card per 30 minutes.

* Surveys, beta testing, card sorts, and tree tests: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift cards, or a GitLab swag item. 

* Design evaluations: Unpaid.

Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

When attempting to send incentives internationally, your credit card or PayPal may be denied. You can offer to send an Amazon.com card (not in the user's country's store) or use [transferwise](https://transferwise.com/), which requires bank routing information.